package com.paypay.currencyexchange.core

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider

/**
 * The factory class that provides instance of ViewModel which extends Architecture Components
 * ViewModel.
 *
 * More about it: [Architecture Guide](https://developer.android.com/arch).
 *
 * Created by jakalesmana on 2019-09-11.
 */

class ViewModelFactory @Inject constructor(
    private val mCreators: @JvmSuppressWildcards Map<Class<out ViewModel>,
            @JvmSuppressWildcards Provider<ViewModel>>)
    : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var creator: Provider<out ViewModel>? = mCreators[modelClass]
        if (creator == null) {
            for ((key, value) in mCreators) {
                if (modelClass.isAssignableFrom(key)) {
                    creator = value
                    break
                }
            }
        }
        if (creator == null) {
            throw IllegalArgumentException("unknown model class $modelClass")
        }
        try {
            @Suppress("UNCHECKED_CAST")
            return creator.get() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}