package com.paypay.currencyexchange

import android.app.Activity
import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.paypay.currencyexchange.di.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by jakalesmana on 2019-09-11.
 */
class CurrencyExchangeApp : Application(), HasActivityInjector {

    @Inject
    lateinit var mDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector() = mDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}