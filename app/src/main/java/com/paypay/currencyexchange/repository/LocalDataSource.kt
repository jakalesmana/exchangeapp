package com.paypay.currencyexchange.repository

import com.google.gson.Gson
import com.paypay.currencyexchange.data.ExchangeDataPreference
import com.paypay.currencyexchange.entity.CurrencyList
import com.paypay.currencyexchange.entity.QuoteList
import javax.inject.Inject

/**
 * Created by jakalesmana on 2019-09-12.
 */

class LocalDataSource @Inject constructor(private val preference: ExchangeDataPreference,
                                          private val gson: Gson){

    fun getCurrencyList(): CurrencyList {
        val data = preference.getCurrencyList()
        return gson.fromJson(data, CurrencyList::class.java)
    }

    fun getQuoteList(): QuoteList {
        val data = preference.getQuoteList()
        return gson.fromJson(data, QuoteList::class.java)
    }
}