package com.paypay.currencyexchange.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.paypay.currencyexchange.core.Mockable
import com.paypay.currencyexchange.entity.CurrencyList
import com.paypay.currencyexchange.entity.QuoteList
import com.paypay.currencyexchange.rest.ApiResponse
import com.paypay.currencyexchange.rest.RemoteResource
import com.paypay.currencyexchange.rest.Resource
import com.paypay.currencyexchange.rest.Status
import javax.inject.Inject

/**
 * Created by jakalesmana on 2019-09-11.
 */

@Mockable
class Repository @Inject constructor(private val remoteDataSource: RemoteDataSource,
                                     private val localDataSource: LocalDataSource) {

    fun getCurrencyList(): LiveData<Resource<CurrencyList>> {
        return object : RemoteResource<CurrencyList>() {
            override fun createCall(): LiveData<ApiResponse<CurrencyList>> {
                return remoteDataSource.getCurrencyList()
            }
        }.asLiveData()
    }

    fun getCachedCurrencyList(): LiveData<Resource<CurrencyList>> {
        val res = MutableLiveData<Resource<CurrencyList>>()
        val list = localDataSource.getCurrencyList()
        val data = Resource(Status.SUCCESS, list , null, "")
        res.value = data
        return res
    }

    fun getQuoteList(): LiveData<Resource<QuoteList>> {
        return object : RemoteResource<QuoteList>() {
            override fun createCall(): LiveData<ApiResponse<QuoteList>> {
                return remoteDataSource.getQuoteList()
            }
        }.asLiveData()
    }

    fun getCachedQuoteList(): LiveData<Resource<QuoteList>> {
        val res = MutableLiveData<Resource<QuoteList>>()
        val list = localDataSource.getQuoteList()
        val data = Resource(Status.SUCCESS, list , null, "")
        res.value = data
        return res
    }
}