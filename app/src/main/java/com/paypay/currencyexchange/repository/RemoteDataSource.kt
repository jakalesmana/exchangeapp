package com.paypay.currencyexchange.repository

import android.arch.lifecycle.LiveData
import com.paypay.currencyexchange.di.ApiModule.Companion.API_ACCESS
import com.paypay.currencyexchange.entity.CurrencyList
import com.paypay.currencyexchange.entity.QuoteList
import com.paypay.currencyexchange.rest.ApiResponse
import com.paypay.currencyexchange.rest.AppRest
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by jakalesmana on 2019-09-11.
 */

class RemoteDataSource @Inject constructor(
    private val appRest: AppRest,
    @Named(API_ACCESS) val remoteAccess: String) {

    fun getQuoteList(): LiveData<ApiResponse<QuoteList>> {
        return appRest.getQuoteList(remoteAccess)
    }

    fun getCurrencyList(): LiveData<ApiResponse<CurrencyList>> {
        return appRest.getCurrencyList(remoteAccess)
    }
}