package com.paypay.currencyexchange.entity

/**
 * Created by jakalesmana on 2019-09-11.
 */
class CurrencyList (
    val currencies: Map<String, String>
)