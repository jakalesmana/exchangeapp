package com.paypay.currencyexchange.entity

import java.math.BigDecimal

/**
 * Created by jakalesmana on 2019-09-12.
 */
class QuoteList (
    val quotes: Map<String, BigDecimal>
)