package com.paypay.currencyexchange.base

import android.os.Bundle

/**
 * The contract interface as marker for any classes (Activity, Fragment) that needs to inject
 * extras bundle into it
 *
 * Created by jakalesmana on 2019-09-11.
 */

interface ExtraInjectable {

    /**
     * Override this method to inject the values from extras bundle
     * This method is automatically called in onCreate of Activity or Fragment
     */
    fun injectExtras(extras: Bundle)
}