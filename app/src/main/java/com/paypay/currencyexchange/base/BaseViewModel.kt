package com.paypay.currencyexchange.base

import android.arch.lifecycle.ViewModel

/**
 * The base ViewModel, all common functionalities go here
 *
 * Created by jakalesmana on 2019-09-11.
 */

abstract class BaseViewModel : ViewModel()