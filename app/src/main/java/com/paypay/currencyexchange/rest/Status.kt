package com.paypay.currencyexchange.rest

/**
 * An enum defines state of [Resource.class][Resource]
 *
 * Created by jakalesmana on 2019-09-11.
 */

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
