package com.paypay.currencyexchange.rest

/**
 * The wrapper class of data, contains current state of the data including [.status],
 * [.message] and [.code]
 *
 * Created by jakalesmana on 2019-09-11.
 */

class Resource<T>(
    /**
     * Contains the current status of data.
     * Please refer to the enum [Status]
     */
    val status: Status,
    /**
     * Contains the wrapped data
     */
    val data: T?,
    /**
     * Contains the integer code reflecting to the state of data
     */
    val code: Int?,
    /**
     * Additional information message regarding to state of data
     * Usually used in case of data retrieving was failed
     */
    val message: String?
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val resource = other as Resource<*>?

        if (status != resource!!.status) {
            return false
        }
        if (if (message != null) message != resource.message else resource.message != null) {
            return false
        }
        if (if (code != null) code != resource.code else resource.code != null) {
            return false
        }
        return if (data != null) data == resource.data else resource.data == null
    }

    override fun hashCode(): Int {
        var result = status.hashCode()
        result = 31 * result + (message?.hashCode() ?: 0)
        result = 31 * result + (code?.hashCode() ?: 0)
        result = 31 * result + (data?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Resource{" +
                "status=" + status +
                ", message='" + message + '\''.toString() +
                ", code=" + code +
                ", data=" + data +
                '}'.toString()
    }

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null, null)
        }

        fun <T> error(code: Int?, msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, code, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null, null)
        }
    }
}