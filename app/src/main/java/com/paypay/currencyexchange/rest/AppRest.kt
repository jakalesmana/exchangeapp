package com.paypay.currencyexchange.rest

import android.arch.lifecycle.LiveData
import com.paypay.currencyexchange.core.Mockable
import com.paypay.currencyexchange.entity.CurrencyList
import com.paypay.currencyexchange.entity.QuoteList
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by jakalesmana on 2019-09-11.
 */

@Mockable
interface AppRest {

    @GET("list")
    fun getCurrencyList(@Query("access_key") access: String):
        LiveData<ApiResponse<CurrencyList>>

    @GET("live")
    fun getQuoteList(@Query("access_key") access: String):
            LiveData<ApiResponse<QuoteList>>

}