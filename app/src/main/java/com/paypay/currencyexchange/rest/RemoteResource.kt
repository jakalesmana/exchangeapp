package com.paypay.currencyexchange.rest

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.support.annotation.MainThread

/**
 * A generic class that can provide a resource backed by the network only without any additional
 * processing logic of network result
 *
 * Created by jakalesmana on 2019-09-11.
 */

abstract class RemoteResource<ResultType> @MainThread protected constructor() {

    private val mResult = MediatorLiveData<Resource<ResultType>>()

    init {
        mResult.value = Resource.loading(null)

        val apiResponse = createCall()
        mResult.addSource(apiResponse) { response ->
            if (response!!.isSuccessful()) {
                setValue(Resource.success(response.body))
            } else {
                onFetchFailed()
                setValue(
                    Resource.error(
                        response.code,
                        response.errorMessage ?: "",
                        null
                    )
                )
            }
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (!equals(mResult.value, newValue)) {
            mResult.value = newValue
        }
    }

    private fun equals(a: Any?, b: Any): Boolean {
        return a === b || a != null && a == b
    }

    /**
     * Override this method in case custom handling or logic is need when data fetching was failed
     * This method is optional
     */
    protected fun onFetchFailed() {}

    /**
     * Call this method to retrieve result of the resource
     *
     * @return the Resource wrapped result as LiveData
     */
    fun asLiveData(): LiveData<Resource<ResultType>> {
        return mResult
    }

    /**
     * Override this method to passing the API call implementation from RemoteDataSource
     */
    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<ResultType>>
}