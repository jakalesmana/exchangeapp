package com.paypay.currencyexchange.rest

import javax.net.ssl.HttpsURLConnection

/**
 * Created by jakalesmana on 2019-09-11.
 */

class ApiResponse<T> {

    var code = HttpsURLConnection.HTTP_OK

    var body: T? = null

    var errorMessage: String? = null

    fun isSuccessful(): Boolean {
        return code == HttpsURLConnection.HTTP_OK
    }
}