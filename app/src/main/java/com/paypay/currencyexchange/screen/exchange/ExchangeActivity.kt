package com.paypay.currencyexchange.screen.exchange

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.paypay.currencyexchange.BR
import com.paypay.currencyexchange.R
import com.paypay.currencyexchange.base.BaseActivity
import com.paypay.currencyexchange.databinding.ActivityExchangeBinding
import com.paypay.currencyexchange.rest.Status
import java.math.BigDecimal

class ExchangeActivity : BaseActivity<ActivityExchangeBinding, ExchangeViewModel>(),
    AdapterView.OnItemSelectedListener {

    var exchangeAdapter: ExchangeAdapter? = null

    override fun getViewModelBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_exchange
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.getCurrencyListLiveData.observe(this, Observer { resource ->
            if (resource?.status == Status.SUCCESS) {
                resource.data?.let {data ->
                    viewModel.currencyList = data
                    viewModel.getQuoteList()
                } ?: showError()

            } else if (resource?.status == Status.ERROR) {
                showError()
            }
        })

        viewModel.getQuoteListLiveData.observe(this, Observer { resource ->
            if (resource?.status == Status.SUCCESS) {
                resource.data?.let {data ->
                    viewModel.quoteList = data
                    viewModel.saveLatest()

                    updateUi()
                } ?: showError()

            } else if (resource?.status == Status.ERROR) {
                showError()
            }
        })

        viewModel.getCurrencyList()
    }

    private fun showError() {

    }

    private fun updateUi() {
        if(exchangeAdapter == null) {
            exchangeAdapter = ExchangeAdapter(BigDecimal.ZERO, "USD", viewModel.quoteList.quotes)
        }

        val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
            viewModel.getCurrencyArray())
        dataBinding.spinner.apply {
            adapter = arrayAdapter
            onItemSelectedListener = this@ExchangeActivity
        }

        dataBinding.rvExchange.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@ExchangeActivity, 1)
            adapter = exchangeAdapter
        }

        dataBinding.etAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(et: Editable?) {
                dataBinding.rvExchange.visibility =
                    if(et.toString().isEmpty()) View.GONE else View.VISIBLE
                exchangeAdapter?.updateAmount(et.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
        view?.let {
            exchangeAdapter?.updateSource((view as TextView).text.toString())
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}
}
