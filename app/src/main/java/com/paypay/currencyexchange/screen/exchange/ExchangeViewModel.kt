package com.paypay.currencyexchange.screen.exchange

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.google.gson.Gson
import com.paypay.currencyexchange.base.BaseViewModel
import com.paypay.currencyexchange.core.SingleLiveEvent
import com.paypay.currencyexchange.data.ExchangeDataPreference
import com.paypay.currencyexchange.entity.CurrencyList
import com.paypay.currencyexchange.entity.QuoteList
import com.paypay.currencyexchange.repository.Repository
import com.paypay.currencyexchange.rest.Resource
import javax.inject.Inject

/**
 * Created by jakalesmana on 2019-09-11.
 */

const val EXPIRED_TIME_LIMIT = 30 * 60 * 1000 //30 mins
class ExchangeViewModel @Inject constructor(private val repository: Repository,
                                            private val preference: ExchangeDataPreference)
    : BaseViewModel() {

    private val triggerGetCurrencyList = SingleLiveEvent<Boolean>()
    private val triggerGetQuoteList = SingleLiveEvent<Boolean>()

    lateinit var currencyList: CurrencyList
    lateinit var quoteList: QuoteList

    private var retrieveFromCache = false

    @Inject
    lateinit var gson: Gson

    val getCurrencyListLiveData: LiveData<Resource<CurrencyList>> =
        Transformations.switchMap(triggerGetCurrencyList) { fromCache ->
            if(fromCache) {
                repository.getCachedCurrencyList()
            } else {
                repository.getCurrencyList()
            }
    }

    val getQuoteListLiveData: LiveData<Resource<QuoteList>> =
        Transformations.switchMap(triggerGetQuoteList) { fromCache ->
            if(fromCache) {
                repository.getCachedQuoteList()
            } else {
                repository.getQuoteList()
            }
        }

    fun getCurrencyList() {
        val latestUpdate = preference.getLatestUpdate()

        if(System.currentTimeMillis() - latestUpdate > EXPIRED_TIME_LIMIT) {
            triggerGetCurrencyList.value = false
        } else {
            triggerGetCurrencyList.value = true
            retrieveFromCache = true
        }
    }

    fun getQuoteList() {
        triggerGetQuoteList.value = retrieveFromCache
    }

    fun saveLatest() {
        if(!retrieveFromCache) {
            preference.setLatestUpdateMillis(System.currentTimeMillis())
            preference.setCurrencyList(gson.toJson(currencyList))
            preference.setQuoteList(gson.toJson(quoteList))
        }
    }

    fun getCurrencyArray() : Array<String> {
        return currencyList.currencies.keys.toTypedArray()
    }

}