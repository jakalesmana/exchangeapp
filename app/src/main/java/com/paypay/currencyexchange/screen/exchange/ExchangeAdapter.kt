package com.paypay.currencyexchange.screen.exchange

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.paypay.currencyexchange.R
import java.math.BigDecimal
import java.math.MathContext

/**
 * Created by jakalesmana on 2019-09-12.
 */
class ExchangeAdapter(private var amount: BigDecimal,
                      private var source: String,
                      private val quotes: Map<String, BigDecimal>) :
    RecyclerView.Adapter<ExchangeAdapter.ExViewHolder>() {

    private var multiplier = BigDecimal.ZERO
    private var quoteKeys = quotes.keys.toTypedArray()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExViewHolder {
        val textView = LayoutInflater.from(parent.context).inflate(R.layout.list_text_item, parent,
            false) as TextView
        return ExViewHolder(textView)
    }

    fun updateSource(source: String) {
        this.source = source
        multiplier = quotes["USD$source"] ?: BigDecimal.ZERO
        notifyDataSetChanged()
    }

    fun updateAmount(amount: String) {
        if(amount.isEmpty()) {
            this.amount = BigDecimal.ZERO
        } else {
            this.amount = amount.toBigDecimal()
        }

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return quotes.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ExViewHolder, position: Int) {
        val key = quoteKeys[position]

        val value: BigDecimal
        if(multiplier.compareTo(BigDecimal.ZERO) == 0) {
            value = BigDecimal(0)
        } else {
            value = quotes[key]?.times(amount)?.divide(multiplier, MathContext.DECIMAL64)
                ?: BigDecimal.ZERO
        }

        holder.textView.text = "${key.substring(3)} $value"
    }

    class ExViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView)
}