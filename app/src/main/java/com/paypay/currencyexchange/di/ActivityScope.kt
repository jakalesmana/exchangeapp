package com.paypay.currencyexchange.di

import javax.inject.Scope

/**
 * A dagger custom scope of such dependencies is tied to the scope of Activity
 *
 * More about it: [DependencyInjection with Dagger 2]
 * (https://github.com/codepath/android_guides/wiki/Dependency-Injection-with-Dagger-2).
 *
 * Created by jakalesmana on 2019-09-11.
 */

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope