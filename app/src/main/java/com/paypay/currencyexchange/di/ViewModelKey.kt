package com.paypay.currencyexchange.di

import android.arch.lifecycle.ViewModel
import dagger.MapKey
import kotlin.reflect.KClass

/**
 * A custom identifies annotation types that are used to associate ViewModel class as keys with
 * ViewModel Provider as values returned by [com.paypay.currencyexchange.core.ViewModelFactory]
 *
 * @see [Map multibinding](https://google.github.io/dagger/multibindings.map-multibindings)
 *
 * Created by jakalesmana on 2019-09-11.
 */

@MustBeDocumented
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)