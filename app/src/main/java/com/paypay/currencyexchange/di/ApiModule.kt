package com.paypay.currencyexchange.di

import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by jakalesmana on 2019-09-11.
 */

@Module
class ApiModule {

    companion object {
        const val HOST_URL = "hostUrl"
        const val API_ACCESS = "apiAccess"
    }

    @Singleton
    @Provides
    @Named(HOST_URL)
    fun provideHostUrl(): String {
        return "http://apilayer.net/api/"
    }

    @Singleton
    @Provides
    @Named(API_ACCESS)
    fun provideApiAccess(): String {
        return "d51c22b4b8c4b6e2b703172241302cba"
    }
}