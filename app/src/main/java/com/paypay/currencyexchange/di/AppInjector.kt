package com.paypay.currencyexchange.di

import com.paypay.currencyexchange.CurrencyExchangeApp

/**
 * Created by jakalesmana on 2019-09-11.
 */
object AppInjector {

    fun init(application: CurrencyExchangeApp) {
        DaggerAppComponent
            .builder()
            .application(application)
            .build()
            .inject(application)

    }
}