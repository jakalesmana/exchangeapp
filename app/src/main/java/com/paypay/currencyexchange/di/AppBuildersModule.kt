package com.paypay.currencyexchange.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.paypay.currencyexchange.core.ViewModelFactory
import com.paypay.currencyexchange.screen.exchange.ExchangeActivity
import com.paypay.currencyexchange.screen.exchange.ExchangeViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import javax.inject.Singleton

/**
 * Created by jakalesmana on 2019-09-11.
 */

@Module
abstract class AppBuildersModule {

    @Singleton
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeExchangeActivity(): ExchangeActivity

    @Binds
    @IntoMap
    @ViewModelKey(ExchangeViewModel::class)
    abstract fun bindExchangeViewModel(exchangeViewModel: ExchangeViewModel): ViewModel
}