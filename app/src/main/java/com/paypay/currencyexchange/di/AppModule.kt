package com.paypay.currencyexchange.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.paypay.currencyexchange.di.ApiModule.Companion.HOST_URL
import com.paypay.currencyexchange.rest.AppRest
import com.paypay.currencyexchange.rest.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by jakalesmana on 2019-09-11.
 */

@Module(includes = [AppBuildersModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }

    @Singleton
    @Provides
    fun provideAppRest(@Named(HOST_URL) hostUrl: String): AppRest {

        return Retrofit.Builder()
            .baseUrl(hostUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(AppRest::class.java)
    }
}