package com.paypay.currencyexchange.data

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by jakalesmana on 2019-09-12.
 */

@Singleton
class ExchangeDataPreference @Inject constructor(context: Context) {

    companion object {
        private const val NAME = "ExchangeData"
        private const val LATEST_UPDATE_KEY = "LatestUpdate"
        private const val CURRENCY_LIST_KEY = "CurrencyList"
        private const val QUOTE_LIST_KEY = "QuoteList"
    }

    private var mPref: SharedPreferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE)

    private var mEditor: SharedPreferences.Editor? = null

    fun setLatestUpdateMillis(millis: Long) {
        doEdit()
        mEditor!!.putLong(LATEST_UPDATE_KEY, millis)
        doApply()
    }

    fun getLatestUpdate() : Long {
        return mPref.getLong(LATEST_UPDATE_KEY, 0L)
    }

    fun setCurrencyList(data: String) {
        doEdit()
        mEditor!!.putString(CURRENCY_LIST_KEY, data)
        doApply()
    }

    fun setQuoteList(data: String) {
        doEdit()
        mEditor!!.putString(QUOTE_LIST_KEY, data)
        doApply()
    }

    fun getCurrencyList() : String {
        return mPref.getString(CURRENCY_LIST_KEY, "")!!
    }

    fun getQuoteList() : String {
        return mPref.getString(QUOTE_LIST_KEY, "")!!
    }

    @SuppressLint("CommitPrefEdits")
    private fun doEdit() {
        if (mEditor == null) {
            mEditor = mPref.edit()
        }
    }

    private fun doApply() {
        if (mEditor != null) {
            mEditor!!.apply()
            mEditor = null
        }
    }
}