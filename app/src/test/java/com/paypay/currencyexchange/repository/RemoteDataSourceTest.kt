package com.paypay.currencyexchange.repository

import com.paypay.currencyexchange.rest.AppRest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by jakalesmana on 2019-09-12.
 */

@RunWith(MockitoJUnitRunner::class)
class RemoteDataSourceTest {

    lateinit var remoteDataSource: RemoteDataSource

    @Mock
    lateinit var appRest: AppRest

    private val remoteAccess = "remoteAccess"

    @Before
    fun setUp() {
        remoteDataSource = RemoteDataSource(appRest, remoteAccess)
    }

    @Test
    fun when_getQuoteList_Then_CallRestQuoteList() {
        remoteDataSource.getQuoteList()

        verify(appRest).getQuoteList(remoteAccess)
    }

    @Test
    fun when_getCurrencyList_Then_CallRestCurrencyList() {
        remoteDataSource.getCurrencyList()

        verify(appRest).getCurrencyList(remoteAccess)
    }
}